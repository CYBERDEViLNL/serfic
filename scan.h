#include <net/if.h>
#include <iwlib.h>

// -- STRUCTS
typedef struct angle_data{
	unsigned char angle_start;	// start angle in degrees
	unsigned char angle_stop;	// stop angle in degrees
	unsigned char tics	;		// steps
} angle_data;

struct angles_data{
	struct angle_data h;		// horizontal angle in degrees
	struct angle_data v;		// vertical angle in degrees
} angles_data;

typedef struct scan_data{
	unsigned char bssid[6];		// BSSID
	char essid[32];				// ESSID
	int8_t signal;				// average signal
	uint8_t channel;			// channel 1 - 13
								//encryption
								//pairwise ciphers
								//auth suites
} scan_data;

typedef struct scan_entry{
	unsigned char cur_h_angle;
	unsigned char cur_v_angle;
	struct scan_data data;
} scan_entry;

typedef struct scan_sock{
	int socket;
	char if_name[IFNAMSIZ];
	iwrange range;
	wireless_scan_head head;
} scan_sock;

// -- VARS
size_t entry_count;

// -- FUCNTIONS
void init_scan(scan_sock *scan_obj);
void close_sockets(int socket);
void scan(scan_sock sock, scan_data ** entries);
