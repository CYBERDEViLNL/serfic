# serfic [WIP]
Servo controlled wifi scanner written in C
currently supports Arduino (src: ```https://github.com/CYBERDEViLNL/serfi/blob/master/servo_arduino.ino```)

# compile
```
gcc -o serfic serfic.c scan.c arduino_com.c -liw -lpthread
```

# arguments

```
serfic [OPTIONS] [WIRELESS_INTERFACE_NAME]

OPTIONS
-h 'START_ANGLE, STOP_ANGLE, STEPS'
		Horizontal

-v 'START_ANGLE, STOP_ANGLE, STEPS'
		Vertical

-s [SCAN_AMOUNT]
		Preform scan
		SCAN_AMOUNT: The amount of scans it should preform (per step if angle arguments are set).

-m 'AXIS, ANGLE'
		TODO! Move servo to angle.

-o FILE
		TODO! Write ouput to file, if not set it will print to stdout.

-V LEVEL
		Verbose
		level 1 - print data for every step
		level 2 - print only final results
		
-f PATTERN
		WIP! Output format
			%HORIZONTAL_ANGLE%
			%VERTICAL_ANGLE%
			%BSSID%
			%ESSID%
			%CHANNEL%
			%SIGNAL%

		Example:
			-f '%HORIZONTAL_ANGLE%, %VERTICAL_ANGLE%, %BSSID%, %SIGNAL%\n'

-i WIRELESS_INTERFACE_NAME
		TODO Example: -i wlan0

-a ARDUINO_PATH
		Example: -a /dev/ttyUSB0
    
Example:
  ./serfic -h0,180,10 -v50,110,10 -s3 -V1 -a/dev/ttyUSB0 wlan0
```
