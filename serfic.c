//gcc -o serfic serfic.c scan.c arduino_com.c -liw -lpthread
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <pthread.h>
#include <math.h>
#include "serfic.h"

void process_results(scan_data *scan_results, scan_entry** final_results, unsigned char cur_h_angle, unsigned char cur_v_angle){
	int i;
	int z;

	int final_bssid_entry;
	int b;
	for(i = 0; i < entry_count; i++){
		final_bssid_entry = -1;
		for(b = 0; b < final_results_count; b++){
			// if bssid in final_results
			if(strncmp(scan_results[i].bssid, (*final_results)[b].data.bssid, sizeof(scan_results[i].bssid)) == 0){
				final_bssid_entry = b;
				break;
			}
		}
		if(final_bssid_entry == -1){ // new entry
			*final_results = (scan_entry *) realloc(*final_results, (final_results_count+1) * sizeof(scan_entry)); // add another struct to the array.
			if(final_results == NULL){
				printf("could not allocate new struct to array\n");
				exit(0);
			}

			(*final_results)[final_results_count].cur_h_angle = cur_h_angle;
			(*final_results)[final_results_count].cur_v_angle = cur_v_angle;

			strncpy((*final_results)[final_results_count].data.bssid, scan_results[i].bssid, sizeof((*final_results)[final_results_count].data.bssid));
			strncpy((*final_results)[final_results_count].data.essid, scan_results[i].essid, sizeof((*final_results)[final_results_count].data.essid));

			(*final_results)[final_results_count].data.channel = scan_results[i].channel;
			(*final_results)[final_results_count].data.signal = scan_results[i].signal;

			final_results_count++;

		} else { // entry exists, update
			if(scan_results[i].signal > (*final_results)[final_bssid_entry].data.signal){ // if new signal is better
				// update signal and angles
				(*final_results)[final_bssid_entry].data.signal = scan_results[i].signal;
				(*final_results)[final_bssid_entry].cur_h_angle = cur_h_angle;
				(*final_results)[final_bssid_entry].cur_v_angle = cur_v_angle;
			}
		}
	}
}

angle_data parse_angle_args(char *optarg, angle_data dat){
	unsigned short tmp;
	char *strtoul_tmp = NULL;
	char * arg = strtok(optarg,",");
	while(arg != NULL){
		tmp = strtoul(arg, &strtoul_tmp, 0);
		if(dat.angle_start == 181){
			if(tmp >= 0 && tmp <= 180){
				dat.angle_start = tmp;
			} else{
				printf("\nstart angle argument must be between 0 and 180\n");
				exit(0);
			}
		} else if(dat.angle_stop == 181){
			if(tmp >= 0 && tmp <= 180){
				dat.angle_stop = tmp;
			} else{
				printf("\nstop angle argument must be between 0 and 180\n");
				exit(0);
			}
		} else if(dat.tics == 181){
			if(tmp >= 0 && tmp <= 90){
				dat.tics = tmp;
			} else{
				printf("\ntics argument must be between 0 and 90\n");
				exit(0);
			}
		}
		arg = strtok(NULL,",");
	}
	return dat;
}

void parse_servo_data_args(char *optarg, servo_data *data){
	
	char *strtoul_tmp = NULL;
	char * arg = strtok(optarg,",");
	data->axis = 'a';
	while(arg != NULL){
		if(data->axis == 'a'){
			data->axis = arg[0];
		} else{
			data->angle = strtoul(arg, &strtoul_tmp, 0);
		}
		arg = strtok(NULL,",");
	}
}

void print_scan_entries(scan_entry * entries){
	//system("clear");
	usleep(10000);
	printf("ID) HORI VERT CH dBm BSSID              ESSID\n");
	int i;
	for(i = 0; i < final_results_count; i++){//final_results_count is defined in serfic.h
		printf("%02d) %3d° %3d° %02d %d %02X:%02X:%02X:%02X:%02X:%02X\t`%s`\n",
		i+1,
		entries[i].cur_h_angle,
		entries[i].cur_v_angle,
		entries[i].data.channel,
		entries[i].data.signal,
		entries[i].data.bssid[0],
		entries[i].data.bssid[1],
		entries[i].data.bssid[2],
		entries[i].data.bssid[3],
		entries[i].data.bssid[4],
		entries[i].data.bssid[5],
		entries[i].data.essid);
	}
	printf("%c",output_separator);
}

void print_scan_data(scan_data * data, unsigned char cur_h_angle, unsigned char cur_v_angle){
	int i;
	for(i = 0; i < entry_count; i++){//final_results_count is defined in serfic.h
		printf("%02d) %3d° %3d° %02d %d %02X:%02X:%02X:%02X:%02X:%02X\t`%s`\n",
		i+1,
		cur_h_angle,
		cur_v_angle,
		data[i].channel,
		data[i].signal,
		data[i].bssid[0],
		data[i].bssid[1],
		data[i].bssid[2],
		data[i].bssid[3],
		data[i].bssid[4],
		data[i].bssid[5],
		data[i].essid);
	}
	printf("%c",output_separator);
}

void print_formated_scan_enrties(scan_entry * entries, char * fmt){
	int i,y;
	int count = 0;
	bool loop = false;
	char *tmp = (char *) malloc(0 * sizeof(char));
	for(i = 0; i < final_results_count; i++){//final_results_count is defined in serfic.h
		for(y = 0; y < strlen(fmt); y++){
			if(fmt[y] == '%'){
				if(loop == true){
					loop = false;
					if(strncmp(tmp, "HORIZONTAL_ANGLE", strlen("HORIZONTAL_ANGLE")) == 0){
						printf("%d", entries[i].cur_h_angle);
					} else if(strncmp(tmp, "VERTICAL_ANGLE", strlen("VERTICAL_ANGLE")) == 0){
						printf("%d", entries[i].cur_v_angle);
					} else if(strncmp(tmp, "BSSID", strlen("BSSID")) == 0){
						printf("%02X:%02X:%02X:%02X:%02X:%02X",
							entries[i].data.bssid[0],
							entries[i].data.bssid[1],
							entries[i].data.bssid[2],
							entries[i].data.bssid[3],
							entries[i].data.bssid[4],
							entries[i].data.bssid[5]
						);
					} else if(strncmp(tmp, "ESSID", strlen("ESSID")) == 0){
						printf("%s",entries[i].data.essid);
					} else if(strncmp(tmp, "SIGNAL", strlen("SIGNAL")) == 0){
						printf("%d",entries[i].data.signal);
					} else if(strncmp(tmp, "CHANNEL", strlen("CHANNEL")) == 0){
						printf("%d",entries[i].data.channel);
					} else if(strncmp(tmp, "COUNT", strlen("COUNT")) == 0){
						printf("%d",i+1);
					}
					//reset
					count = 0;
					tmp = (char *) malloc(0 * sizeof(char));
				} else {
					loop = true;
				}
			} else{
				if(loop == true){
					tmp = realloc(tmp, (count+1) * sizeof(char));
					tmp[count] = fmt[y];
					count++;
				} else{
					// TODO check if \n or \t
					// strncpy does escape those characters
					printf("%c",fmt[y]);
				}
			}
		}
		printf("\n");
	}
	printf("%c",output_separator);
	fflush(stdout);// TODO if stdout is piped (tty is linebuffered, pipe is fully buffered)
}

void *serfic_scan(void *args){
	// TODO check if wireless interface is set and valid.
	//..

	// init scan, open sock etc
	serfic_scan_args *t_args = args;
	init_scan(&t_args->scan_obj);

	// init serial communication to arduino
	int arduino;
	arduino = init_fd(t_args->arduino_path);
	
	if(arduino == -1){
		goto EXIT;
	}

	printf("wait a sec for arduino to reboot.\n");
	sleep(1); // let the arduino reboot.

	servo_data servo_instruction_x;
	servo_data servo_instruction_y;
	
	final_results_count = 0;// declared in serfic.h
	int final_bssid_entry;
	int z;
	//int servo_steps = 2;

	unsigned char h_tics = 0;
	unsigned char v_tics = 0;
	if(t_args->instructions.angle.h.tics > 0){
		h_tics = ceil((t_args->instructions.angle.h.angle_stop - (t_args->instructions.angle.h.angle_start )) / t_args->instructions.angle.h.tics);
	}
	if(t_args->instructions.angle.v.tics > 0){
		v_tics = ceil((t_args->instructions.angle.v.angle_stop - (t_args->instructions.angle.v.angle_start )) / t_args->instructions.angle.v.tics);
	}
	//printf("v_tics %d\n",v_tics);
	//printf("h_tics %d\n",h_tics);
	
	printf("Ok start scanning.\n");
	for(unsigned char iv = 0; iv <= v_tics; iv++){//vertical
		servo_instruction_y.axis = 'y';
		servo_instruction_y.angle = t_args->instructions.angle.v.angle_start + (iv * t_args->instructions.angle.v.tics);
		move(arduino, &servo_instruction_y);
		usleep(1000000);
		//printf("iv %d\n",iv);
		for(unsigned char ih = 0; ih <= h_tics; ih++){
			servo_instruction_x.axis = 'x';
			servo_instruction_x.angle = t_args->instructions.angle.h.angle_start + (ih * t_args->instructions.angle.h.tics);			

			move(arduino, &servo_instruction_x);
			usleep(1000000);
			
			t_args->cur_h_angle = servo_instruction_x.angle;
			t_args->cur_v_angle = servo_instruction_y.angle;

			// reinit
			entry_count = 0;
			scan_data * scan_results = (scan_data *) malloc(0 * sizeof(scan_data));;

			for(z = 0; z < t_args->instructions.scan_amount; z++){ // scan x times for signal comparison
				scan(t_args->scan_obj, &scan_results); // do scan and fill array of structs
			}

			// process results 
			process_results(
				scan_results,
				&t_args->final_results,
				(unsigned char)servo_instruction_x.angle,
				(unsigned char)servo_instruction_y.angle
			);

			if(t_args->verbose_level == 1){
				// TODO
				//printf("%d,%d\n",servo_instruction_x.angle,servo_instruction_y.angle);
				serfic_scan_update = true;
				while(serfic_scan_update == true){
					usleep(1000*10);//10ms
				}
			}

			// free scan_results array
			free(scan_results);

		}
	}
	if(t_args->verbose_level == 2){
		//TODO
		//printf("X:%d\tY:%d\n",servo_instruction_x.angle,servo_instruction_y.angle);
		serfic_scan_update = true;
		while(serfic_scan_update == true){
			usleep(1000*10);//10ms
		}
	}

EXIT:

	// free final_results array
	free(t_args->final_results);

	// close serial communication to arduino
	destroy(arduino);

	// close socket
	close_sockets(t_args->scan_obj.socket);
	
	serfic_scan_runs = false;
	
}

void move_servo(char * arduino_path, servo_data data){
	int arduino = init_fd(arduino_path);
	if(arduino == -1){
		goto EXIT;
	}
	usleep(1000000); // let the arduino reboot.
	move(arduino, &data);
	//usleep(500000);
	
EXIT:
	destroy(arduino);
}

/* serfic [OPTIONS] WIRELESS_INTERFACE_NAME
 * 
 * OPTIONS
 * -h 'START_ANGLE, STOP_ANGLE, STEPS'
 * 			Horizontal
 * 
 * -v 'START_ANGLE, STOP_ANGLE, STEPS'
 * 			Vertical
 * 
 * -s [SCAN_AMOUNT]
 * 			Preform scan
 * 			SCAN_AMOUNT: The amount of scans it should preform (per step if angle arguments are set).
 * 
 * -m 'AXIS, ANGLE'
 * 			TODO! Move servo to angle.
 * 
 * -o FILE
 * 			TODO! Write ouput to file, if not set it will print to stdout.
 * 
 * -v LEVEL
 * 			Verbose
 * 			level 0 - print data for every scan PRINTS NON-PROCESSCED DATA
 * 			level 1 - print data for every step
 * 			level 2 - print only final results
 * 			
 * -f PATTERN
 * 			WIP! Output format
 * 				%HORIZONTAL_ANGLE%
 * 				%VERTICAL_ANGLE%
 * 				%BSSID%
 * 				%ESSID%
 * 				%CHANNEL%
 * 				%SIGNAL%
 * 
 * 			Example:
 * 				-f '%HORIZONTAL_ANGLE%, %VERTICAL_ANGLE%, %BSSID%, %SIGNAL%\n'
 * 
 * 
 * -i WIRELESS_INTERFACE_NAME
 * 			TODO Example: -i wlan0
 * 
 * -a ARDUINO_PATH
 * 			TODO Example: -a /dev/ttyUSB0
 * 
 * -c CONFIG_PATH
 * 			TODO You can set various options as default in the config file
 * 			Example `serfic.conf`
 * 			{
 * 			}
 * 			Example: -c /etc/serfic.conf
 * */
int main(int argc,char** argv){	
	// init values
	serfic_scan_args args;
	servo_data servo_dat;
	args.instructions.angle.h.angle_start = 181;
	args.instructions.angle.h.angle_stop = 181;
	args.instructions.angle.h.tics = 181;
	args.instructions.angle.v.angle_start = 181;
	args.instructions.angle.v.angle_stop = 181;
	args.instructions.angle.v.tics = 181;
	args.instructions.scan_amount = 0;
	args.verbose_level = 2;//serfic.h
	args.arduino_path = (char *) malloc(strlen("/dev/ttyUSB0") * sizeof(char)+1);
	strncpy(args.arduino_path, "/dev/ttyUSB0", strlen("/dev/ttyUSB0"));
	output_separator = '\n'; // defined in serfic.h
	

	char *strtoul_tmp = NULL;
	char c;
	while ((c = getopt (argc, argv, "h:v:s:i:a:V:f:m:")) != -1) {
		switch(c){
			case 'h':
				if(optarg){
					args.instructions.angle.h = (angle_data) parse_angle_args(optarg, args.instructions.angle.h);
				}
				break;
			case 'v':
				if(optarg){
					args.instructions.angle.v = (angle_data) parse_angle_args(optarg, args.instructions.angle.v);
				}
				break;
			case 's'://scan amount
				if(optarg){
					args.instructions.scan_amount = strtoul(optarg, &strtoul_tmp, 0);
				} else {
					args.instructions.scan_amount = 1;//default
				}
				break;
			case 'V':
				// TODO check if 0, 1 or 2
				if(optarg){
					args.verbose_level = strtoul(optarg, &strtoul_tmp, 0);
				}
				break;
			case 'f':// output format per entry
				if(optarg){
					if(strlen(optarg) < 1024){
						output_entry_format = (char *) malloc(strlen(optarg) * sizeof(char)+1); // string functions add a termination byte.
						strncpy(output_entry_format, optarg, strlen(optarg)+1);
					}
				}
				break;
			case 'i'://set interface
				// TODO
				break;
			case 'a'://set arduino path
				args.arduino_path = (char *) malloc(strlen(optarg) * sizeof(char)+1); // string functions add a termination byte.
				strncpy(args.arduino_path, optarg, strlen(optarg)+1);
				break;
			case 'm'://move servo
				// TODO
				//servo_data data;
				
				parse_servo_data_args(optarg, &servo_dat);
				move_servo(args.arduino_path, servo_dat);
				
				break;
		}
	}

	/*printf("\n\nH start: %d\n",instructions.angle.h.angle_start);
	printf("H stop: %d\n",instructions.angle.h.angle_stop);
	printf("H tics: %d\n",instructions.angle.h.tics);

	printf("\nV start: %d\n",instructions.angle.v.angle_start);
	printf("V stop: %d\n",instructions.angle.v.angle_stop);
	printf("V tics: %d\n\n",instructions.angle.v.tics);*/

	if(args.instructions.scan_amount > 0){
		if(optind && argv[optind]){
			strncpy(args.scan_obj.if_name, argv[optind], sizeof(args.scan_obj.if_name));
		}
		
		scan_entry * final_results = (scan_entry *) malloc(0 * sizeof(scan_entry));
		args.final_results = (scan_entry *) malloc(0 * sizeof(scan_entry));
		
		int err;
		pthread_t thread_id;
		err = pthread_create(&thread_id, NULL, &serfic_scan, &args);
		if(err != 0){
			printf("failed to create thread.\n");
		} else {
			printf("thread created.\n");
			serfic_scan_runs = true;
			while(serfic_scan_runs == true){
				if(serfic_scan_update == true){// it (was) a race condition.
					if(output_entry_format){
						printf("CURPOS X%d Y%d\n",args.cur_h_angle,args.cur_v_angle);
						print_formated_scan_enrties(args.final_results, output_entry_format);
					} else {
						printf("CURPOS X%d Y%d\n",args.cur_h_angle,args.cur_v_angle);
						print_scan_entries(args.final_results);
					}
					serfic_scan_update = false;// unblock
				}
				usleep(1000*10);//10ms
			}
		}
	}
	return 0;
}
