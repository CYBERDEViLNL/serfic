#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <stdbool.h>

#include "arduino_com.h"

/* Sources:
 *	http://man7.org/linux/man-pages/man3/termios.3.html
 *	https://en.wikibooks.org/wiki/Serial_Programming/termios#open.282.29
 *	https://www.gnu.org/software/libc/manual/html_node/Example-of-Getopt.html
 * 	https://www.gnu.org/software/libc/manual/html_node/Signal-Handling.html
 *	http://man7.org/linux/man-pages/man3/strtol.3.html
 * */

char opcode = (char)OP_SERVO;
char exec = (char)EXEC_BYTE;
char term = (char)TERM_BYTE;
char datok = (char)DATA_OK;

unsigned char cur_x_angle = 181;
unsigned char cur_y_angle = 181;

char read_serial(int fd){
	char buf;
	read(fd, &buf, sizeof(char));
	return buf;
}

void write_servo(int fd, servo_data *instruction){
	write(fd,&opcode,1);				// send opcode
	while(read_serial(fd) != opcode){
		usleep(10000);
	}// loop until confirmation

	write(fd,&instruction->axis,1);		// send axis data
	write(fd,&instruction->angle,1);	// send angle data
	write(fd,&exec,1);					// send execute byte
	while(read_serial(fd) != exec){
		usleep(10000);
	}// loop until confirmation
	while(read_serial(fd) != datok){
		usleep(10000);
	}// loop until confirmation
}

bool file_exists(const char * file){
	if(access( file, F_OK ) == -1){
		return false;
	} else {
		return true;
	}
}

bool file_write_access(const char * file){
	if(access( file, W_OK ) == -1){
		return false;
	} else {
		return true;
	}
}

int open_fd(const char * device, int oflags){
	int fd = open(device, oflags);
	if(fd == -1) {
		printf("failed to open port\n");
		return -1;
	} else {
		printf("connection opened: %d %s\n",fd,device);
		return fd;
	}
}

int init_fd(char * device){
		struct termios config;

		int fd; // file descript0r
		if(file_exists(device) == false){
			printf("`%s` does not exist\n", device);
			return -1;
		}
		if(file_write_access(device) == false){
			printf("no write access to `%s`\n", device);
			return -1;
		}

		fd = open_fd(device, O_RDWR);
		if(fd == -1) {
			printf("could not open `%s`\n", device);
			return -1;
		}

		if(!isatty(fd)){// check if tty
			printf("not tty\n");
			return -1;
		}
		if(tcgetattr(fd, &config) < 0){// get current termios config
			printf("could not get termios attributes\n");	
			return -1;
		}
		// set the baudrate
		if(cfsetispeed(&config, B9600) < 0 || cfsetospeed(&config, B9600) < 0){
			printf("could not set baudrates");
			return -1;
		}
		
		config.c_cc[VMIN]  = 0; // min chars
		config.c_cc[VTIME] = 5; // 5ms
		config.c_lflag = 0; // non canonical
		
		//if(tcsetattr(fd, TCSAFLUSH, &config) < 0){ // apply new config
		if(tcsetattr(fd, TCSANOW, &config) < 0){ // apply new config
			printf("failed to set new termios attributes");
			return -1;
		}
		return fd;
}

int destroy(int fd){
	close(fd);
	printf("connection closed\n");
	return 0;
}

int move(int fd, servo_data *instruction){
	//printf("move\n");
	if(instruction->axis == 'x' || instruction->axis == 'y'){
		//printf("axis detected\n");
		if(instruction->angle >= 0 && instruction->angle <= 180){
			//printf("axis: %c\t", instruction->axis);
			//printf("angle: %d\n", instruction->angle);
			write_servo(fd,instruction);
			return 0;
		}
	}
	return -1;	
}
