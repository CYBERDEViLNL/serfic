#include "scan.h"

#define ARDUINO 0
#ifdef ARDUINO
	#include "arduino_com.h"
#endif
#ifdef RASPBERRY
	#include "rpi_com.h"
#endif

// -- VARS
size_t final_results_count;
//uint8_t verbose_level;
char * output_entry_format;
char output_separator;
bool serfic_scan_update;
bool serfic_scan_runs;

// -- STRUCTS
typedef struct scan_instructions{
	struct angles_data angle;
	unsigned char scan_amount;	// do `scan_amount` scans per step
} scan_instruction;

typedef struct serfic_scan_args{
	scan_instruction instructions;
	scan_sock scan_obj;
	unsigned char cur_h_angle;
	unsigned char cur_v_angle;
	scan_entry * final_results;
	uint8_t verbose_level;
	char *arduino_path;
} serfic_scan_args;

// -- FUNCTIONS
void *serfic_scan(void *args);
