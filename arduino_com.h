#define TERM_BYTE	0x00
#define EXEC_BYTE	0x01
#define OP_SERVO	0x02
#define DATA_OK		0x03

// -- STRUCTS
typedef struct servo_data{
	char axis;
	unsigned char angle;
} servo_data;

// -- FUNCTIONS
int open_fd(const char * device, int oflags);
int init_fd(char * device);
int destroy(int fd);
int move(int fd, servo_data *instruction);
