#include <stdio.h>
//#include <time.h>
#include <stdlib.h>

#include "scan.h"

/* https://gist.github.com/rosenhouse/685e6c4c7f3b1332aa0f
* https://github.com/HewlettPackard/wireless-tools/blob/master/wireless_tools/iwlib.h
* https://github.com/torvalds/linux/blob/master/include/uapi/linux/wireless.h
* 
* struct wireless_scan_head	https://github.com/HewlettPackard/wireless-tools/blob/master/wireless_tools/iwlib.h#L255
* struct wireless_scan		https://github.com/HewlettPackard/wireless-tools/blob/master/wireless_tools/iwlib.h#L235
* struct iw_range 			https://github.com/torvalds/linux/blob/master/include/uapi/linux/wireless.h#L953
* struct iw_statistics		https://github.com/torvalds/linux/blob/master/include/uapi/linux/wireless.h#L875
* 
* struct iw_param			https://github.com/torvalds/linux/blob/master/include/uapi/linux/wireless.h#L674
* 
* https://sourcecodebrowser.com/wireless-tools/28/globals_func_0x69.html
* 
* https://github.com/torvalds/linux/blob/master/include/uapi/linux/if.h#L32
* https://github.com/torvalds/linux/blob/master/include/uapi/linux/if.h#L232
* #define	IFNAMSIZ		16
* #define	IFHWADDRLEN		6
* */

char * set_interface(){
	//https://www.systutorials.com/docs/linux/man/3-if_nameindex/
	unsigned int if_index;
	char * if_name = (char *) malloc(IFNAMSIZ);
	struct if_nameindex *if_ni, *i;
	if_ni = if_nameindex();

	if (if_ni == NULL) {
		perror("if_nameindex");
		exit(EXIT_FAILURE);
	}

	for (i = if_ni; ! (i->if_index == 0 && i->if_name == NULL); i++){
		printf("%u: %s\n", i->if_index, i->if_name);
	}

	printf("Please select the interface you wish to use: ");

	// need to look at this later!
	// never trust user input xD
	scanf("%d", &if_index);
	printf("selected interface %d\n", if_index);

	i = if_ni + (if_index - 1);
	strcpy(if_name, i->if_name);

	if_freenameindex(if_ni);

	return if_name;
}

void init_scan(scan_sock *scan_obj){
	entry_count = 0;

	if(strlen(scan_obj->if_name) == 0){
		strncpy(scan_obj->if_name, set_interface(), sizeof(scan_obj->if_name));
	} else{
		//strncpy(scan_obj->if_name, if_name, sizeof(scan_obj->if_name));
		// just do check if IF is valid
	}

	printf("if: %s\n", scan_obj->if_name);

	scan_obj->socket = iw_sockets_open();

	/* Get some metadata to use for scanning */
	if (iw_get_range_info(scan_obj->socket, scan_obj->if_name, &scan_obj->range) < 0) {
		printf("Error during iw_get_range_info. Is this a wireless interface? Aborting.\n");
		exit(2);
	}
}

void scan(scan_sock sock, scan_data ** entries) {
	wireless_scan *result;
	if (iw_scan(sock.socket, sock.if_name, sock.range.we_version_compiled, &sock.head) < 0) {
		printf("Error during iw_scan. Aborting.\n");
		exit(2);
	}

	result = sock.head.result;
	int i;

	while (NULL != result) {
		int bssid_entry = -1;
		for(i = 0; i < entry_count; i++){
			if(strncmp(result->ap_addr.sa_data, (*entries)[i].bssid, sizeof((*entries)[i].bssid)) == 0){
				bssid_entry = i;
				break; // break the for loop, we know enough for now.
			}
		}
		if(bssid_entry == -1){ // new entry
			if(result->has_ap_addr){
				*entries = (scan_data *) realloc(*entries, (entry_count+1) * sizeof(scan_data)); // add another struct to the array.
				strncpy((*entries)[entry_count].bssid, result->ap_addr.sa_data, sizeof((*entries)[entry_count].bssid));
			} else {
				continue;
			}

			strncpy((*entries)[entry_count].essid, result->b.essid, sizeof((*entries)[entry_count].essid));
			(*entries)[entry_count].channel = iw_freq_to_channel(result->b.freq, &sock.range);

			if(result->has_stats){
				(*entries)[entry_count].signal = result->stats.qual.level;
			}
			entry_count++;

		} else { // update average signal
			// average signal
			(*entries)[bssid_entry].signal = ((*entries)[bssid_entry].signal + (int8_t)result->stats.qual.level) / 2;
		}

		result = result->next;

	}
}

void close_sockets(int socket){
	iw_sockets_close(socket);
}
